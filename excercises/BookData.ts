// One Book Data - only what we actualy use

export interface BookData {
    kind: "books#volume"; // tagged union discrimnator (tag)
    id: string;
    volumeInfo: {
        title: string;
        subtitle?: string;
        authors: string[];
        publishedDate: string;
        pageCount: number;
        categories: string[];
    };
}
export interface CreateBookData {
    title?: string;
    subtitle?: string;
    authors?: string[];
    publishedDate?: string;
    pageCount?: number;
    categories?: string[];
}
export interface UpdateBookData {
    id: string;
    volumeInfo: {
        title?: string;
        subtitle?: string;
        authors?: string[];
        publishedDate?: string;
        pageCount?: number;
        categories?: string[];
    };
}
// Fixture - example / test data

export let booksData: BookData[] = [
    {
        "kind": "books#volume",
        "id": "Y-mUDwAAQBAJ",
        "volumeInfo": {
            "title": "Programming TypeScript",
            "subtitle": "Making Your JavaScript Applications Scale",
            "authors": ["Boris Cherny"],
            "publishedDate": "2019-04-25",
            "pageCount": 322,
            "categories": ["Computers"],
        }
    },
    {
        "kind": "books#volume",
        "id": "UZB352QcfiMC",
        "volumeInfo": {
            "title": "TypeScript Revealed",
            "authors": ["Dan Maharry"],
            "publishedDate": "2013-01-28",
            "pageCount": 83,
            "categories": ["Computers"],
        }
    },
    {
        "kind": "books#volume",
        "id": "jSbZDQAAQBAJ",
        "volumeInfo": {
            "title": "Alice's Adventures in Wonderland",
            "authors": ["Lewis Carroll"],
            "publishedDate": "2020-10-29",
            "pageCount": 200,
            "categories": [
                "Fiction"
            ],
        }
    }
];
