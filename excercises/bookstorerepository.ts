import { BookData, booksData, CreateBookData } from "./BookData"

export { }



abstract class ICriteria {
    filter(value: IBook, index: number, array: IBook[]): boolean {
        // return true
        throw new Error('Criteria filter method not implemented')
    }
}
abstract class Sort { }

// new ICriteria() // Cannot create an instance of an abstract class

class QueryByTitle extends ICriteria {
    constructor(private _title: string) { super() }
    filter(book: IBook) { return book.title.toLocaleLowerCase().includes(this._title.toLocaleLowerCase()) }
}

interface IBooksRepository {
    findById(id: Book['id']): IBook | undefined
    findAll(): IBook[]
    query(criteria: ICriteria, sort?: Sort): IBook[]
    create(bookPayload: CreateBookData): IBook
    save(book: any): IBook
    delete(bookId: IBook['id']): void
}


interface IBook {
    id: string
    title: string
    authors: IterableIterator<string> | string[]
    publishedDate: Date | null
    toJSON(): BookData
}

class Book implements IBook {
    readonly kind = "books#volume" as const
    private _title = ''
    private _authors = new Set<string>()
    private _publishedDate: Date | null = null

    get title() { return this._title }
    set title(title) {
        if (!title) throw 'Invalid Title'
        this._title = title
    }

    get authors() { return Array.from(this._authors.values()) }
    addAuthor(author: string) { return this._authors.add(author) }
    get publishedDate() { return this._publishedDate }
    set publishedDate(dateString: Date | null) {
        if (dateString === null) { this._publishedDate = null; return; }
        const publishedDate = new Date(dateString)
        if (isNaN(publishedDate.valueOf())) { throw 'Invalid date' }
        this._publishedDate = publishedDate
    }

    private constructor(readonly id: string) { }

    // Singletons
    // static _instance?: IBook

    // static getInstance(): IBook {
    //     return Book._instance ?? (this._instance = new Book('123'))
    // }

    static createFromJSON(data: BookData['volumeInfo']) {
        const { title, authors, categories, pageCount, publishedDate } = data
        const newBook = new Book(Date.now().toString())
        newBook.title = title
        newBook.addAuthor(authors[0])
        return newBook;
    }
    static loadFromJSON(data: BookData) {
        const newBook = new Book(data.id)
        const { title, authors, categories, pageCount, publishedDate } = data.volumeInfo
        newBook.title = title
        newBook.addAuthor(authors[0])
        return newBook
    }

    toJSON() {
        return {
            kind: this.kind,
            id: this.id,
            volumeInfo: {
                title: this.title,
                authors: Array.from(this.authors),
                publishedDate: this.publishedDate?.toLocaleString() || '',
                pageCount: 0,
                categories: [],
            }
        }
    }
}


class MemoryBooksRepository implements IBooksRepository {
    constructor(private _books: IBook[] = []) { }

    findById(id: string): IBook | undefined {
        return this._books.find(book => book.id === id)
    }
    findAll(): IBook[] {
        return this._books.slice()
    }
    query(criteria: ICriteria, sort?: Sort): IBook[] {
        return this._books.filter(criteria.filter)
    }
    create(bookPayload: CreateBookData): IBook {
        return Book.createFromJSON({
            "title": " ",
            "authors": [],
            "publishedDate": " ",
            "pageCount": 0,
            "categories": [
            ],
            ...bookPayload
        })
    }
    save(book: IBook): IBook {
        const existing = this.findById(book.id)
        if (!existing) {
            this._books.push(book)
        }
        return book
    }
    delete(bookId: string) {
        const index = this._books.findIndex(book => book.id === bookId)
        if (index !== -1) {
            this._books.splice(index, 1)
        }
    }
}

const repo = new MemoryBooksRepository()
booksData.forEach(book => {
    const existingBook = Book.loadFromJSON(book)
    repo.save(existingBook)
})
// console.log(repo.findAll().map(book => book.title))
console.log(repo.query(new QueryByTitle('Alice')).map(book => book.title))
// repo.findById(id)
// repo.query(criteria, sort)
// repo.create( bookData )
// repo.save ( book )