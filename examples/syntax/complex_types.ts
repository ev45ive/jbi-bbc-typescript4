

// const item: {
//     name: string;
//     score: number;
// } = {
//     name: 'The Item',
//     score: 42
// }

interface Item {
    name: string
    /**
     * Latest game score
     */
    score: number
}

type AlsoAnItem = {
    name: string
    /**
     * Latest game score
     */
    score: number
}

const item: Item = {
    name: 'The Item',
    score: 42
}


type ItemModes = 'simple' | 'advanced' | 'ridicuolus'

function iProduceItem(): AlsoAnItem {
    return item
}

function iConsumeItem(item: Item, mode: ItemModes) {
    return item.name + ' : ' + item.score
}

iConsumeItem(iProduceItem(), 'ridicuolus')


// ## Dynamic complex types in interfaces - Type unions
{
    interface Item42 {
        id: string | number
        name: string
    }

    // const item = {} as Item42 // Dont do it in production!

    function showItem(item: Item42): string {
        // item.id as string // unsafe
        const stringId1 = String(item.id) // safe 
        const stringId2 = item.id.toString() // safe 

        if (typeof item.id === 'string') {
            return item.id.toLocaleUpperCase()
        } else {
            return item.id.toFixed(2)
        }
    }

    const item = {} as Item42 // User Input / Server response
    // const x:string = showItem(item).toString()
    const result = showItem(item)
    if ('string' === typeof result) {
        const x: string = result
    }
}

// ## Incomplete complex types 
{
    interface Track {
        id: string
        name: string
    }

    // interface PlaylistFull{
    // interface PlaylistSimplified{

    // type Playlist = {
    //     id: string
    //     name: string
    // } | {
    //     id: string
    //     name: string
    //     tracks: Track[]
    // }

    interface Playlist {
        id: string | number
        name: string
        // tracks: Track[] | undefined
        tracks?: Track[]
    }

    function showPlaylist(result: Playlist): string {

        // if (result.tracks !== undefined) { return result.name + result.tracks.length }
        // else { return result.name + ' (no tracks)' }

        // return result.name + (result.tracks ? result.tracks.length : ' (no tracks)')

        // return result.name + (result.tracks && result.tracks.length || ' (no tracks)')

        let num1 = result.tracks?.length //  number | undefined
        let num2 = result.tracks?.length || 0 // number

        let num3 = (result.tracks as Track[]).length  // number  (type assertion!) // unsafe
        let num4 = result.tracks!.length  // number  (non-optional assertion!) // unsafe

        // let tracks1 = result.tracks ? result.tracks  :  []
        // let tracks2 = result.tracks ?? []
        // let num = (result.tracks ?? []).length

        return result.name + (result.tracks)?.length || ' (no tracks)'

    }

}

// ## Structural typing ( aka. Duck Typing ) [ not nominal Typing ala Java ]
{
    interface Vector { x: number, y: number, length: number }
    interface Point { x: number, y: number }

    let v: Vector = { x: 123, y: 123, length: 124 }
    let p: Point = { x: 123, y: 123 }

    p = v
    // v = p // Property 'length' is missing in type 'Point' but required in type 'Vector'.

    // Data hiding // Exapsulation // Polymorphism
}

// ## Complex type Unions - discriminate by shape
{
    interface Track {
        id: string
        name: string
        duration: number
    }

    interface Playlist {
        id: string
        name: string
        tracks?: Track[]
    }

    type Result = Track | Playlist

    function showResult(result: Track | Playlist) {
        if ('duration' in result) {
            // 'My Playlist (20 tracks)'
            return `${result.name} (${result.duration}ms)`
        } else if (result.tracks) {
            // 'My Song (02:34)'
            return `${result.name} (${result.tracks?.length} tracks)`
        }
    }
}

// ## Complex type - Tagged Unions - discriminate by tag
{
    interface Track {
        id: string
        name: string
        duration: number
        type: 'track'
    }
    interface Podcast {
        id: string
        name: string
        duration: number
        type: 'podcast'
    }

    interface Playlist {
        id: number
        name: string
        tracks?: Track[]
        type: 'playlist'
    }

    type Result = Track | Playlist  // | Podcast
    // type TrackThatIsAlsoAPlaylisty = Track & Playlist  
    // const hybrid: TrackThatIsAlsoAPlaylisty = {};

    const checkExhaustiveness = (result: never): never => { throw new Error('Unsuported result type ' + result) }

    function showResult(result: Result) {
        // if(result.type == 'playlist')
        switch (result.type) {
            case 'track':
                return `${result.name} (${result.duration}ms)`
            case 'playlist':
                return `${result.name} (${result.tracks?.length} tracks)`
            // case 'podcast':
            //     return `${result.name} (${result.duration}ms)`
            default:
                // const _neverShouldHappen: never = result // Compiletime check
                // throw new Error('Unsuported result type') // Runtime check
                return checkExhaustiveness(result)
        }
    }
}

// ## Type guard

// typeof result === 'string'
// if(result) // not undefined
// if(result !== undefined) // not undefined

// else .... // rest of union cases
// case: ... default: .... // rest of union cases
// if(..) { return ... } .... // rest of union cases

// const result = new Playlist() 
// if(result instanceof Playlist) // class not a interface

// 'property' in result // discriminate by shape - does it have property?
// result.property === 'unique_key' // discriminate by tag - item has a 'label'


// ## Readonly
{
    interface Point {
        readonly x: number;
        readonly y: number;
    }
    // After the assignment, x and y can’t be changed.
    let p1: Point = { x: 10, y: 20 };
    let p2 = Object.freeze({ x: 10, y: 20 })
    // p1.x = 5; // Error: Cannot assign to 'x' because it is a read-only property.
}

// # Indexed types
{
    interface StringArray {
        length: number;
        [index: number]: string;
    }
    let myArray: StringArray;
    // myArray = ["Bob", "Fred"];
    myArray = { 0: 'Bob', length: 123 };
    myArray[1] = 'Fred'
    // myArray['x'] = 'Fred' // Error!
    // myArray.length // Property 'length' does not exist on type 'StringArray'
    let myStr: string = myArray[0];


    interface Point {
        readonly x: number;
        readonly y: number;
    }
    interface PointsStore {
        [playlistId: string]: Point
        // [Point] : string // A computed property name in an interface must refer to an expression whose type is a literal type or a 'unique symbol' type.
    }
    const store: PointsStore = {
        '123': { x: 123, y: 324 },
        [123 + '123']: { x: 123, y: 324 },
    }
    store['123'].x
    store[123].x
    store[3].x // JS error x of undefined
    store[3]?.x
}
// ## Extracting parts of types
{
    interface User { id: string }
    type UserID = User["id"];

    // function find(id: string): Object { return {} as Object }

    function findUser(id: User["id"]): User { return {} as User }
    findUser('playlist_id') // maybe? JS runtime error?
}

// ## Dynamic Indexed Type + Extracting property (lookup) - Mapped Types
{
    type RequestCredentials =
        | "omit"
        | "same-origin"
        | "include";

    let cred: RequestCredentials;
    cred = "omit"; // OK
    // cred = "inclade"; // Error!

    type Headers = 'Accept' | 'Authorization'
    type ExtraHeaders = 'X-Proxy'
    type AllHeaders = ExtraHeaders | Headers
    const headers: AllHeaders = 'X-Proxy'

    // interface RequestHeaders1 {
    //     [header: 'Accept' | 'Authorization']: string // Consider using a mapped object type instead.ts(1337)
    // }

    type RequestHeaders2 = {
        // [header: string]: string
        // [header in 'Accept' | 'Authorization']: string
        [header in AllHeaders]?: string
    }
    const headers2: RequestHeaders2 = {
        "X-Proxy": '123',
        // "pancakse":'123'
    }
}

// === Interface vs Types

type BetterName = string | number
type BetterObjectName = { x: number, y: number }
type BetterObjectName2 = { x: number, y: number, tag: 'typeA' } | { length: number, tag: 'typeB' }

interface myBaseObj { base: number }
interface MyObject extends myBaseObj {
    x: number, y: number
}

type MyObject2 = myBaseObj & {
    x: number, y: number
}
// // const x: MyObject = { base: 123, x: 123, y: 123 }
// const x: MyObject2 = { base: 123, x: 123, y: 123 }

// Plugins, Hooks, interceptors, extenstions, modules

// type MyObject2 = {} // Error: Duplicate identifier 'MyObject2'.
interface myBaseObj { extraPluginInfo: true } // OK - Declaration Merging

// // const x: MyObject = { base: 123, x: 123, y: 123 }
const x: MyObject2 = { base: 123, x: 123, y: 123, extraPluginInfo: true }

// ## Namespaces

const myNamespace = (function () {
    var privateVar = 123;

    function method() {
        return privateVar;
    }

    return { method }
})()
myNamespace.method() // 123
// myNamespace.privateVar // Property 'privateVar' does not exist on type 'typeof myNamespace'.ts

namespace myNamespace2 {
    var privateVar = 123;

    export function method() {
        return privateVar;
    }
    export namespace Nested {
        export const nestedStuff = 1
    }
}
myNamespace2.method() // 123
// myNamespace2.privateVar // Error property 'privateVar' does not exist on type 'typeof myNamespace2'
myNamespace2.Nested.nestedStuff // 1

// import {method, stuff, otherstuff, myNamespace} from './any-module'
// myNamespace.nested.thins


interface X {
    a: number
    z: { value: number }
}
interface Y {
    a: number
    b: { value: string }
}

// OR - Union - Exclusive Union
type XorY = X | Y
function test1(xory: XorY) {
    xory.a // 'a' exists on X and on Y
    // xory.b // Property 'b' does not exist on type 'XorY'. // Property 'b' does not exist on type 'X'.
}

// AND - Intersection (Inclusive Union)
type XandY = X & Y
function test2(xory: XandY) {
    xory.a // 'a' exists on X and on Y
    xory.b // 'b' exists on product of X and on Y
    xory.z // 'z' exists on product of X and on Y
}
type PartXPartY = {
    a: X['a'] & Y['a'],
    z: X['z'] | Y['b']
}
const part: PartXPartY = {
    a: 123, z: { value: '123' }
}