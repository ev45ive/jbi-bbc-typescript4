"use strict";
exports.__esModule = true;
var x = 42;
var THE_ANSWER = 42;
var MAGIC;
(function (MAGIC) {
    MAGIC[MAGIC["LOW"] = 0] = "LOW";
    MAGIC[MAGIC["MEDIUM"] = 1] = "MEDIUM";
    MAGIC[MAGIC["HIGH"] = 2] = "HIGH";
})(MAGIC || (MAGIC = {}));
var levelofMagic1 = MAGIC.HIGH;
var levelofMagicUltra = 5;
var HTTP_CODES;
(function (HTTP_CODES) {
    HTTP_CODES[HTTP_CODES["OK"] = 200] = "OK";
    HTTP_CODES[HTTP_CODES["BAD_REQUEST"] = 400] = "BAD_REQUEST";
    HTTP_CODES[HTTP_CODES["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    HTTP_CODES[HTTP_CODES["PAYMENT_REQUIRED"] = 402] = "PAYMENT_REQUIRED";
    HTTP_CODES[HTTP_CODES["FORBIDDEN"] = 403] = "FORBIDDEN";
    HTTP_CODES[HTTP_CODES["I_AM_A_TEAPOT"] = 418] = "I_AM_A_TEAPOT";
})(HTTP_CODES || (HTTP_CODES = {}));
// const responseStatus: HTTP_CODES = 5;
var responseStatus = 418;
HTTP_CODES['I_AM_A_TEAPOT']; // 418 
HTTP_CODES[responseStatus] == 'I_AM_A_TEAPOT';
// enum InvoiceStatus {
//     SUBMITTED, // 0
//     APPROVED, // 1
//     PAID, // 2
// }
var InvoiceStatus;
(function (InvoiceStatus) {
    InvoiceStatus["APPROVED"] = "APPROVED";
    InvoiceStatus["SUBMITTED"] = "SUBMITTED";
    InvoiceStatus["PAID"] = "PAID";
    // SPECIAL = GETSPECIALCODE()
})(InvoiceStatus || (InvoiceStatus = {}));
var invoiceStatus = InvoiceStatus.PAID;
InvoiceStatus['PAID'] == 'PAID';
function getStatusLabel(status) {
    switch (status) {
        case InvoiceStatus.SUBMITTED:
            return "invoice was submitted";
        case InvoiceStatus.APPROVED:
            return "it's approved";
        case InvoiceStatus.PAID:
            return "invoice paid";
    }
}
var FileAccess;
(function (FileAccess) {
    FileAccess[FileAccess["None"] = 1] = "None";
    FileAccess[FileAccess["Read"] = 2] = "Read";
    FileAccess[FileAccess["Write"] = 4] = "Write";
    FileAccess[FileAccess["Execute"] = 8] = "Execute";
    FileAccess[FileAccess["ReadWrite"] = 6] = "ReadWrite";
})(FileAccess || (FileAccess = {}));
var result = 2 & FileAccess.None;
var role = 'user';
var UserRole;
(function (UserRole) {
    UserRole["USER"] = "user";
    UserRole["ADMIN"] = "admin";
    UserRole["MODERATOR"] = "moderator";
})(UserRole || (UserRole = {}));
;
(function (UserRole) {
    UserRole.checkRole = function () { };
})(UserRole || (UserRole = {}));
UserRole.checkRole();
function getPermissionsFor(role) {
    switch (role) {
        case UserRole.ADMIN:
            return 123;
        case UserRole.USER:
            return undefined;
        case UserRole.MODERATOR:
            return undefined;
        default:
            var _never = role; // exhaustiveness check
            return _never;
    }
}
