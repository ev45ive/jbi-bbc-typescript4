```js

function Person(name){
    this.name = name
}
Person.prototype.sayHello = function(){ return this.name }

alice = new Person('Alice')

function Employee(name, salary){
    Person.apply(this,[name]);
    this.salary = salary
}

Employee.prototype = Object.create( Person.prototype )
Employee.prototype.work = function(){ return this.salary }

tom = new Employee('Tom', 1200)

tom 

```

```js

class Person{
    static legalAge = 21
    age = null
    
    constructor(name){
        this.name = name
    }

    sayHello(){ return 'I am ' +this.name }
}

class Employee extends Person{
    constructor(name, salary){
        super(name)
        this.salary = salary
    }
    sayHello(){ return super.sayHello() + ' and I am busy' }
    work(){ return this.salary }
}
tom = new Employee('Tom', 1200)

// Employee
//     age: null
//     name: "Tom"
//     salary: 1200
//     [[Prototype]]: Person
//         constructor: class Employee 
//         sayHello: ƒ sayHello()
//         work: ƒ work()
//             [[Prototype]]: Object
//                 constructor: class Person
//                 sayHello: ƒ sayHello()
//                     [[Prototype]]: Object

tom.work()
// 1200

tom.sayHello()
// 'I am Tom and I am busy'

```
