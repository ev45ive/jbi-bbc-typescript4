export { }


type MagicNumbers = 1234 | 1331 | 42;
const x: MagicNumbers = 42;
const THE_ANSWER = 42;

enum MAGIC {
    LOW, MEDIUM, HIGH
}
const levelofMagic1: MAGIC = MAGIC.HIGH;
const levelofMagicUltra: MAGIC = 5;


enum HTTP_CODES {
    OK = 200,
    BAD_REQUEST = 400,
    UNAUTHORIZED, // 401,
    PAYMENT_REQUIRED,  // 402
    FORBIDDEN,
    I_AM_A_TEAPOT = 418
}

// const responseStatus: HTTP_CODES = 5;
const responseStatus: HTTP_CODES = 418;
HTTP_CODES['I_AM_A_TEAPOT'] // 418 
HTTP_CODES[responseStatus] == 'I_AM_A_TEAPOT'


// enum InvoiceStatus {
//     SUBMITTED, // 0
//     APPROVED, // 1
//     PAID, // 2
// }
enum InvoiceStatus {
    APPROVED = 'APPROVED',
    SUBMITTED = 'SUBMITTED',
    PAID = 'PAID',
    // SPECIAL = GETSPECIALCODE()
}
const invoiceStatus: InvoiceStatus = InvoiceStatus.PAID
InvoiceStatus['PAID'] == 'PAID'

function getStatusLabel(status: InvoiceStatus) {
    switch (status) {
        case InvoiceStatus.SUBMITTED:
            return "invoice was submitted";
        case InvoiceStatus.APPROVED:
            return `it's approved`;
        case InvoiceStatus.PAID:
            return "invoice paid";
    }
}

enum FileAccess {
    None = 1 << 0,
    Read = 1 << 1,
    Write = 1 << 2,
    Execute = 1 << 3,
    ReadWrite = FileAccess.Read | FileAccess.Write,
}
const result = 0b00010 & FileAccess.None


type Role = "user" | "admin" | "moderator"

const role:Role = 'user'

enum UserRole {
    USER = "user",
    ADMIN = "admin",
    MODERATOR = "moderator",
};
namespace UserRole{
    export const checkRole = function(){}
}
UserRole.checkRole()

function getPermissionsFor(
    role: UserRole,
): number | undefined {
    switch (role) {
        case UserRole.ADMIN:
            return 123;
        case UserRole.USER:
            return undefined;
        case UserRole.MODERATOR:
            return undefined;
        default:
            const _never: never = role; // exhaustiveness check
            return _never
    }
}
