{
    // function not(fn){
    //     return function(x){
    //         return !fn(x) 
    //     }
    // };
    const not = (fn: Function) => (x: any) => !fn(x);

    // isEven = function(x){ return x % 2 === 0 };
    // isEven = (x) => { return x % 2 === 0 };
    const isEven = (x: number) => x % 2 === 0;

    // isOdd = function(x){ return x % 2 !== 0 };
    const isOdd = not(isEven)
    // add = function(x,y){ return x + y }
    // add = function(x) { return function(y){ return x+y } };
    // add = (x) => { return (y) => { return x+y } };
    const add = (x: number) => (y: number) => x + y;

    [1, 2, 3].filter(isOdd).map(add(5))
}

// Inline function types
function myAdd1(x: number, y: number): number { return x + y; };

// Inline function declaration
let myAdd2: (baseValue: number, increment: number) => number = function (x, y) { return x + y; };
myAdd2 = myAdd1
let res = myAdd2(1, 2) // number


// Type alias function declaration
type MyAddFuncType = (baseValue: number, increment: number) => number;
let myAdd3: MyAddFuncType = function (x, y) { return x + y; };


// Interface function declaration
interface MyAddFunc {
    (baseValue: number, increment: number): number
}
let myAdd4: MyAddFunc = function (x, y) { return x + y; };

// typings.d.ts - ambiend Types for JavaScript code
declare let iAmInJs: (baseValue: number, increment: number) => number;
iAmInJs(2, 3) // TS - ok, JS - runtimer error 


// ## Optional, Default, Overloads

function buildName1(firstName: string, lastName: string) { }
// let result1 = buildName1("Bob"); // error, too few parameters
// let result2 = buildName1("Bob", "Adams", "Sr."); // error, too many parameters

function buildName2(firstName = 'Guest', lastName?: string) { firstName }
let result1 = buildName2(); // OK, firstname == 'Guest'
let result2 = buildName2("Bob"); // OK, lastName == undefined

function buildName3(lastName?: string, firstName = 'Guest') {
    lastName?.length
}

// ## ...Spread, ...Rest
{
    function sum(initialValue = 0, ...rest: number[]): number {
        return rest.reduce((sum, x) => sum + x, initialValue)
    }
    sum(1, 2, 3, 4) // ...rest
    sum(...[1, 2, 3, 4]) // ...spread
}


// ## Function 'this' context
{
    function WhatIsThis(this: HTMLButtonElement | HTMLDivElement, ev: MouseEvent) {
        console.log(this.tagName, ev.currentTarget as HTMLButtonElement | HTMLDivElement)
        if (this instanceof HTMLButtonElement) {
            this.form
        }
    }

    const button = document.createElement('button')
    // button.onclick = WhatIsThis;
    button.addEventListener('click', WhatIsThis)
    button.click();
}