export { }

interface IPerson {
    name: string
    sayHello(): string
}

class Person implements IPerson {
    // name: string // strictPropertyInitialization (strict:true)
    // name: string 
    // readonly name = ''
    // name = ''

    // fancyName = this.name.toLocaleLowerCase()

    constructor(public name: string) { }

    // constructor(name: string) {
    // this.name = name
    // }

    sayHello() {
        return 'I am ' + this.name
    }
}

const alice = new Person('Alice')
// alice.name.toString()
// alice.name = 'Pwnd!'


class Employee extends Person {

    constructor(readonly name: string) {
        // parent constructor call is required!
        super(name)
    }

    sayHello(): 'HEllo' {
        // optionally can call overriden methods
        super.sayHello()
        return 'HEllo'
    }
}

interface ITakePersonContructors {
    (ctr: new (name: string) => IPerson): IPerson
}
let imakepeople: ITakePersonContructors = (ctr) => {
    return new ctr('Alice')
}
const p1 = imakepeople(Person)
const p2 = imakepeople(Employee)
p2.sayHello()