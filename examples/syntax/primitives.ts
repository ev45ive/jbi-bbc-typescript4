
// ## boolean

// let isThisBookOkay: boolean = true;
let isThisBookOkay = true;

isThisBookOkay = true
// isThisBookOkay = 1 // Error
isThisBookOkay = false
// isThisBookOkay = 0 // Error

// ## number

// let result: number = 123 + 0xbeef + 0b1111 + 0o700;
let result = 123 + 0xbeef + 0b1111 + 0o700;
// 49465
result += 12
result += parseInt('32')
result += Number('32')
// result += 'apples' as number 

// Boolean('1')

// x = new Number(12) 
// Number {12} // Don`t 

// ## string

const firstName: string = 'Mateusz';
const lastName: string = "Kulesza";
let hello: string = `Hello,
${firstName} ${lastName}!`;
// Hello,\nMateusz Kulesza!

// hello = 123 // error
hello = (123).toString(2)

// ## any vs unknown
{
    let anything: any = '123'
    anything = 123
    anything = true
    // anything.????

    let unknownResult: unknown = '123'
    unknownResult = 'another uknown'
    // result.toString() // Object is of type 'unknown'.

    let ihopeitsX = unknownResult as string;

    if (typeof unknownResult === 'string') {
        let concreteResult = parseInt(unknownResult)
    }

    function getValue(key: string): any {
        // return void
    }

    anything = getValue(anything)

    let somethingStatic1 = anything.get.me.a.million.dollars() // any
    let somethingStatic2: number = anything.get.me.a.million.dollars()
    somethingStatic2 // number
}

// ## void

let nothing1: void = undefined;
// let nothing2: void = null; // null is a value!
let nothing3: void = void (0)

// ## null vs undefined

function css(key: string, value?: string | null) {
    if (value === undefined) {
        const mode = 'read value'
    }
    if (value === null) {
        const mode = 'read value'
    }
}

css('color') // read 
css('color', 'red') // write
css('color', null) // write

// ## never 
{
    const name = 'John' // literal string type "John"

    const obj = {
        rabbits: [] as any[],
        items: []
    }
    obj.rabbits[0].dissapear()
    obj.rabbits.push('wabbit')
    obj.items // never[]
    // obj.items[0].x = 1 // Property 'x' does not exist on type 'never'.
    obj.items.length // number

    let _wontHappen: never = {} as never

    function iAlwaysFail(): never {
        throw new Error('Epic failure')
        // return true // Unreachable code detected.
    }

    const failExpression = () => { // infered return type:  never
        throw new Error('Epic failure')
    }

    const neverEndingExpression = () => { // infered return type:  never
        while (true) { }
    }

    function willIReturn() {
        iAlwaysFail()
        return 'success' // Unreachable code detected.
    }

    function iShouldNotBeCalled(_never: never): never {
        throw new Error('This should never happen')
    }

    // if (result !== 'expected')
    //     iShouldNotBeCalled('unexpected value')

}

// ## Object
{
    // typeof x == 'object' 
    const obj0: Object = { x: 1 };
    // obj0.x // Property 'x' does not exist on type 'Object'

    const obj2: {} = { x: 1 };

    const obj1: object = { x: 1 }

    const obj3: { x: number } = { x: 1, }
    obj3.x = 2312

    // typeof null === 'object'

    function isObject(obj: object | null) {
        if (obj !== null) { return true }
    }

    // const obj3: { x: number }  = { x: 1, y: 2 } //Object literal may only specify known properties
}

// ## Array
{
    // Way 1: Type[]
    let list1: number[] = [1, 2, 3];
    // The second way uses a generic array type, Array<elemType>:

    // Way 2: Array<Type>
    let list2: Array<number> = [1, 2, 3];

    // This is NOT and Array! This is a tuple!
    let notAList: [number] = [1]

    let list3 = [1, 2, 3]; //  number[]
    let list4 = [1, 'x', true]; //  (string | number | boolean)[]
    // list4 = ['y', 2, 2, false, false] // Array
}

// ## Tuples
{
    const name1 = 'john'
    let name2 = 'john' as const
    let name3 = 'john' as 'john'
    const obj = {
        x: 1 //as const
    }
    const obj2 = Object.freeze(obj)
    // obj2.x = 2 // there can be only 1

    // let tuple1: [number, string] = [123, 'Alice', '123'] // ERror
    let tuple1: [number, string] = [123, 'Alice']
    let tuple2 = [123, 'Alice'] as const // readonly [123, "Alice"]
    let tuple3: [userId: number, userName: string] = [123, 'Alice']

    tuple3[1] //  string (userName)

    function getPersonInfo1(userId: number, userName: string) { }

    function getPersonInfo2(...myArgs: [userId: number, userName: string]) { }

    getPersonInfo2(123, 'alice') // getPersonInfo(userId: number, userName: string): void

    let notTupleAnymore = tuple3.slice() // (string | number)[]

    const [userId, personName] = tuple3

}
// ## Type Assertions
{

    const name1 = 'john'
    let name2 = 'john' as const
    let name3 = 'john' as 'john'
    let name4 = '12' as unknown as 'john' // I think so... 

    if (name4 === 'john') { // I know so !!!
        let itsJohnForSure = name4 as unknown as 'john'
    }
}