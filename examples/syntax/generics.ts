export { }

const x: string[] = ['a', 'b', 'c'];
const y: Array<string> = ['a', 'b', 'c'];
const z: Array<string | number> = ['a', 123, 'c'];

const arr: Array<Array<string>> = [
    ['X', 'O', 'X'],
    ['O', 'X', 'X'],
    ['X', 'O', 'X'],
    // ['X', 123, 'X'], // Type 'number' is not assignable to type 'string'
];


/* Generics - but not very usefull */
declare function parse<T>(name: string): T
declare function serialize<T>(name: T): string

parse<Array<string>>('anything')
parse('anything') as Array<string>

serialize<string>('anything')
serialize(123)
serialize('string')


/* Generics - proper usage */
type Ref<T> = {
    current: T
}
const ref1: Ref<string> = { current: 'apples' }
const ref2: Ref<number> = { current: 123 }
// ref.current.toString()

function createRef<T>(current: T): Ref<T> {
    return {
        current
    }
}

const ref = createRef('pancakes')
ref.current.toLocaleLowerCase()

/* Collection complex  */
interface GetItemsResponse<T, K> {
    kind: K,
    items: T[]
    total: number
}

/** A class definition with a generic parameter */
class Queue<T> {
    private data: T[] = [];
    push(item: T) { this.data.push(item); }
    pop(): T | undefined { return this.data.shift(); }
}

/* Generic inference - Param is inferred and propagated */

function identity<T>(x: T): T { return x }

function takeFirst<T>(arr: T[]): T { return arr[0] }

const A1: string = takeFirst<string>(['123', '123'])
// const A1: string = takeFirst<string>(['123', 123]) // error
const A2: string | number = takeFirst(['123', 123])

const A3 = takeFirst([1, 2, 4]) // number

const A4: number = takeFirst([1, 2, 4]) /* intelisense: use number[] ! */


/* Generic constructor  */
{
    interface IConstruct<T> {
        new(param: string): T
    }
    class Abc { constructor(name: string) { } doAbc() { } }

    // function build(ctr: IConstruct<Abc>): Abc {
    function build<T>(ctr: IConstruct<T>): T {
        return new ctr('name')
    }
    // build( {} )
    // build( () => new Abc )
    build(Abc).doAbc();
}
/* Instance Type vs Constructor Type */


class Person {
    static getLegalAge() { return 21; }
    constructor(public name: string) { }
};
namespace Person {
    export const xyz = 123
}

Person.xyz
const age = Person.getLegalAge()

// Type of instance created by Person constructor
const alice: Person = new Person('Alice')

alice instanceof Person

type whatIsAlice = typeof alice // Person

// Type of Person class / constructor
const personCtr: typeof Person = Person

personCtr.getLegalAge()
personCtr.xyz
new personCtr('Bob')

// ## Constraints on Generics (extends)
// type ObjWithName = { name: string };

function printName<T extends { name: string }>(arg: T) { return arg.name }

printName({ name: "Kate" }); // OK
printName({ name: "Michael", age: 22 }); // OK
// printName({ age: 22 }); // Error!

function makePair<T, U>(arg1: T, arg2: U): [T, U] {
    return [arg1, arg2];
}

function getKey<T extends object, K extends keyof T>(obj: T, key: K): T[K] {
    return obj[key]
}

enum Role {
    admin = 'admin',
    user = 'user',
}
// type roles = 'admin' | 'user'
type roles = keyof typeof Role

// function getRole(roles: Role, userRole: roles) {
//     return roles[userRole]
// }

function getRole<TRole extends object, UName extends keyof TRole>(roles: TRole, userRole: UName) {
    return roles[userRole]
}
const resulkt = getRole(Role, 'user')

// Conditional and infered types

type params = 'paramA' | 'paramB'
type params2 = 'paramRed' | 'paramBlue'
// type path = `books/${string}/${params}`
type path = `books/${params}/${params2}`
const p: path = 'books/paramA/paramBlue';

type getParams<T extends string, R = T extends `/${infer R}` ? R : {}> = R;

declare function getParam<T extends string, R = getParams<T>>(path: T): R 

getParam('/test')
getParam('test')