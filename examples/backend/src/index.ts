
console.log('Hello NodeJS');

// const http = require('http')
import express from 'express'

const app = express()

// base plugin
app.use((req, res, next) => {
    req.user = { id: 123, name: 'Admin' }
})

// moduleA
app.get('/', (req, res) => {
    // req.user = { id: 123, name: 'Admin' }
    // res.write('Hello ' + (req.user as any).name)
    // res.write('Hello ' + (req.user as User).name)
    res.write('Hello ' + req.user?.name)
    res.end()
})

// moduleB
app.get('/nextpage', (req, res) => {
    // req.user = { id: 123, name: 'Admin' }
    res.write('Hello' + req.user?.name)
    res.end()
})

const PORT = 8080;
const server = app.listen(PORT, 'localhost', () => {
    console.log('Server listening on port ' + PORT);
})

console.log(process.argv);



// node examples/backend/dist/index.js arg1 param2 pancakes
// Hello NodeJS
// [
//   'C:\\Program Files\\nodejs\\node.exe',
//   'C:\\Projects\\szkolenia\\jbi-bbc-typescript-4\\examples\\backend\\dist\\index.js',
//   'arg1',
//   'param2',
//   'pancakes'
// ]
