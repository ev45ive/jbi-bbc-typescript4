
// let x = 1;
// x.toExponential()
// /// <reference path="./index.d.ts" />

import {abc, fun1 ,fun2} from './utils/jsints'

abc(1,2)
fun1(2,3)
fun2(3,4)

export const add = (a: number, b: number) => {
    return a + b;
}

add(1, 2); // Ok
// add("1", 2); // Error
// add(1, "2"); // Error
// add(1/* , undefined */) // Error
// add(1,2,3) // Error
// add.apply(null,[1,2,3]) // Error

export { }

window.document.body;

// process; // NodeJS


[].find(() => {})