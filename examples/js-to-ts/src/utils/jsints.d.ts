// export declare module './utils/jsints' {
    
    export function abc(a: number, b: number): number

    export interface Person {
        name: string
        address: {
            street: string
        }
    }


    // export declare function fun1(a: number, b: number): number;
    export const fun1: (a: number, b: number) => number;

    // export declare const fun1 = (a: number, b: number) => number;

    export function fun2(a: number, b: number): number

// }