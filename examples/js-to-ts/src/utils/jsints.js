// // @ts-nocheck
//  // @ts-ignore

/**
 * Adding function
 * 
 * @export
 * @param {number} a
 * @param {number} b
 * @return {number} 
 */
export function abc(a, b) {
    return a + b
}

// * @param {import('../index').Person} person
// * @param {Person} person

/**
 * Person info
 * @export
 * @param {import('./jsints').Person} person
 * @return {*} 
 */
export function getPersonInfo(person) {
    return person.name + person.address.street
}


/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns number
 */
export const fun1 = (a, b) => a + b;


// @ts-expect-error
export function fun2(a, b) {
    return a + b
}

// /**
//  * @typedef Person
//  * @property {string} name 
//  * @property {Address} address 
// //  * @property {{street:name}} address 
//  */

// /**
//  * @typedef Address
//  * @property {string} street 
//  */


// ../assets/data.json
// ../assets/data.json