```js

myNamespace = (function(){
    var publicstuff = {}
    var privateVar = 123;

    publicstuff.method = function(){
        return privateVar;
    }

    return publicstuff
})()
// {method: ƒ}
myNamespace.method()
// 123

```

```ts
namespace myNamespace{
    var privateVar = 123;

    export var method = function(){
        return privateVar;
    }
}
myNamespace.method()
// 123

```