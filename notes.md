## GIT
cd ..
git clone https://bitbucket.org/ev45ive/jbi-bbc-typescript4.git jbi-bbc-typescript4
cd jbi-bbc-typescript4

cd examples/backend
npm install
npm start

## GIT Update
git pull -u origin master

git stash -u
git pull 

## Installations
node -v 
v14.17.0
https://nodejs.org/en/

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

tsc --version
Version 4.4.3
npm i -g typescript

chrome://version/

Google Chrome	93.0.4577.63 

## Commands

npm i -g typescript

tsc 
tsc --help
tsc --watch file-to-compile.ts
tsc -w file-to-compile.ts
tsc -w -t es2016 --strict script.ts 

## TsConfig.json 
tsc --init 
tsc -t es2016 --strict --init script.ts

message TS6071: Successfully created a tsconfig.json file.

https://www.staging-typescript.org/tsconfig

## Target and Lib
https://github.com/zloirock/core-js


## JS Ts 
// @ts-nocheck
// @ts-expect-error
// @ts-check
// @ts-ignore


## Productivity 
tsc -w
nodemon dist/index.js

https://www.npmjs.com/package/concurrently
concurrently "tsc -w" "nodemon dist/index.js"

ts-node ./index.ts
node  --trace-deprecation -r ts-node/register ./index.ts

nodemon --ext "ts" -w "./src/**" --exec "ts-node src/index.ts"

ts-node --show-config

ts-node-dev --respawn ./src/index.ts

## Deugging
node --inspect ./dist/index.js 
node --inspect -r ts-node/register ./index.ts
node --inspect-brk -r ts-node/register ./index.ts

+ sourceMap
+ inlineSourceMap

## AutoRestart, Recompile, Sourcemaps, etc..
ts-node-dev --respawn --inspect -- ./src/index.ts
Debugger listening on ws://127.0.0.1:9229/3178ffe7-fc4b-43fb-9b21-7f225bb9fa03
For help, see: https://nodejs.org/en/docs/inspector


## Webpack
npm i -g webpack 

webpack init
? Which of the following JS solutions do you want to use? Typescript
? Do you want to use webpack-dev-server? Yes
? Do you want to simplify the creation of HTML files for your bundle? Yes
? Do you want to add PWA support? No
? Which of the following CSS solutions do you want to use? CSS only
? Will you be using PostCSS in your project? No
? Do you want to extract CSS for every file? Only for Production
? Do you like to install prettier to format generated configuration? No
? Pick a package manager: npm
[webpack-cli] ℹ INFO  Initialising project...

npx webpack 
npm run build

npx webpack -w 
npm run watch

npx webpack serve
npm run serve


## React
create-react-app frontend --template typescript
npm start

npm i bootstrap

## Snippets
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets


## Typescript

https://basarat.gitbook.io/typescript/
https://www.typescriptlang.org/docs/handbook/functions.html
