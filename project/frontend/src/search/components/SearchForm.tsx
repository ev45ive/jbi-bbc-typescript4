import React, { useState } from "react";

interface Props {
  onSearch: (query: string) => void;
}

export const SearchForm = ({ onSearch }: Props) => {
  const [query, setQuery] = useState("");

//   const search = (event: React.ChangeEvent<HTMLInputElement>) => {
//     setQuery(event.currentTarget.value);
//   };

  return (
    <div>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          onChange={event => setQuery(event.currentTarget.value)}
          onKeyDown={ event => event.key === 'enter' && onSearch(query)}
        />
        <button
          className="btn btn-outline-secondary"
          type="button"
          onClick={() => {
            onSearch(query);
          }}
        >
          Search
        </button>
      </div>
    </div>
  );
};
