import React from "react";
import { SimpleAlbum } from "../../core/model";

interface Props {
  results: SimpleAlbum[];
}

export const SearchResults = ({ results }: Props) => {
  return (
    <div className="row row-cols-1 row-cols-sm-4 g-0">
      {results.map((result) => {
        return (
          <div className="col" key={result.id}>
            <div className="card">
              <img
                src={result.images[0].url}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h5 className="card-title">{result.name}</h5>
                {/* <p className="card-text">
              This is a longer card with supporting text below as a natural
              lead-in to additional content. This content is a little bit
              longer.
            </p> */}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
