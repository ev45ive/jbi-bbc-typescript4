import React, { useEffect, useState } from "react";
import { SimpleAlbum } from "../core/model";
import { SearchForm } from "./components/SearchForm";
import { SearchResults } from "./components/SearchResults";

interface Props {}

const mockAlbums: SimpleAlbum[] = [
  {
    id: "123",
    name: "Album 123",
    type: "album",
    images: [{ url: "https://www.placecage.com/c/200/300" }],
  },
  {
    id: "234",
    name: "Album 234",
    type: "album",
    images: [{ url: "https://www.placecage.com/c/200/300" }],
  },
  {
    id: "345",
    name: "Album 345",
    type: "album",
    images: [{ url: "https://www.placecage.com/c/200/300" }],
  },
  {
    id: "456",
    name: "Album 456",
    type: "album",
    images: [{ url: "https://www.placecage.com/c/200/300" }],
  },
];

export const MusicSearch: React.FC<Props> = (props) => {
  // const [results, setResults] = useState([] as SimpleAlbum[])
  const [results, setResults] = useState<SimpleAlbum[]>([]);
  const [query, setQuery] = useState("batman");

  useEffect(() => {
    setResults(mockAlbums.filter((item) => item.name.includes(query)));
  }, [query]);

  return (
    <div>
      {/* .container>.row*2>.col */}
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>MusicApp</h1>
            <SearchForm onSearch={(query: string) => setQuery(query)} />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <SearchResults results={results} />
          </div>
        </div>
      </div>
    </div>
  );
};

// MusicSearch.defaultProps = {}
