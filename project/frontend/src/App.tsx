import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import { MusicSearch } from './search/MusicSearch';

function App() {
  return (
    <div className="App">
          <MusicSearch/>
    </div>
  );
}

export default App;
