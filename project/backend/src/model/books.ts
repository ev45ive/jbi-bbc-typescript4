import { BookData, booksData, CreateBookData, UpdateBookData } from "./BookData"

// books.json
interface VolumesListResponse { }

// getBookInfo(book) 'Title - author (date) - subtitle'
export const getBookInfo = ({
    volumeInfo: { title, authors, publishedDate }
}: BookData): string => `${title} by ${authors[0]} (${(formatYear(publishedDate))})`

// findAllBooks() // [...]
export const findAllBooks = () => booksData

// findBookById('123') // {}
export const findBookById = (id: BookData['id']) => findAllBooks().find(book => book.id === id)

// findBookByTitle('123') // {}
export const findBookByTitle = (title: string) => findAllBooks().filter(book => book.volumeInfo.title.includes(title))

// findBookByAuthor('123') // {}
export const findBookByAuthor = (author: string) => findAllBooks().filter(book => book.volumeInfo.authors.includes(author))

export const findBookByCategory = (category: string) => findAllBooks().filter(book => book.volumeInfo.categories.includes(category))

// findBookByYear('123') // {}
export const findBookByYear = (year: string) => findAllBooks().filter(book => formatYear(book.volumeInfo.publishedDate) === year)



// type Criteria = { title: string; author?: string }
type Criteria =
    | { type: 'author', author: string }
    | { type: 'title', title: string }
    | { type: 'year', year: number }
    | { type: 'all' }


export function queryBooks(criteria: { type: 'author', author: string }): BookData[]
export function queryBooks(criteria: { type: 'title', title: string }): BookData[]
/**
 * Search by publish date
 */
export function queryBooks(criteria: { type: 'year', year: number }): BookData[]
export function queryBooks(criteria: { type: 'all' }): BookData[]
export function queryBooks(criteria: Criteria): BookData[] {

    switch (criteria.type) {
        case 'title': return findAllBooks().filter(book => book.volumeInfo.title.includes(criteria.title))
        case 'author': return findAllBooks().filter(book => book.volumeInfo.authors.includes(criteria.author))
        case 'year': return findAllBooks().filter(book => formatYear(book.volumeInfo.publishedDate) === String(criteria.year))
        case 'all': return findAllBooks()
        default:
            const never: never = criteria;
            throw new Error('Invalid criteria')
    }
}



// createBook({...}) // {}
export const createBook = (payload: CreateBookData): BookData => {
    const title = payload.title
    const authors = payload.authors
    if (!title) {
        throw new Error('Title is required')
    }
    if (!authors || authors.length === 0) {
        throw new Error('Authors is required')
    }

    const newBook: BookData = {
        kind: 'books#volume' as const,
        id: Math.ceil(Math.random() * 100_000).toString(),
        volumeInfo: {
            title: title,
            "authors": authors,
            "publishedDate": "",
            "pageCount": 0,
            "categories": [],
            ...payload
        }
    }
    return newBook
}

// updateBook({...}) // {}
export const updateBook = (payload: UpdateBookData): BookData => {
    const book = findBookById(payload.id)
    if (!book) { throw new Error('Book does not exist') }

    const newBook: BookData = {
        kind: 'books#volume' as const,
        id: payload.id,
        volumeInfo: {
            ...book.volumeInfo,
            ...payload.volumeInfo
        }
    }  // dont do  - 'as  BookData'
    return newBook
}

// deleteBook('123') // {}
const deleteBook = (id: BookData['id']) => {
    const index = booksData.findIndex(book => book.id !== id)
    if (~~index) {
        booksData.splice(index, 1)
    }
}

enum SortKey {
    title = 'title',
    pageCount = 'pageCount',
    // authors = 'authors',
    DEFAULT = 'title',
    // , DEFAULT = SortKey.title
}
enum SortDir {
    ASC = 1, DESC = -1, DEFAULT = SortDir.ASC
}

// type BookSorter = {
//     dir: SortDir;
//     key: SortKey;
//     getComparator(): (this: BookSorter, a: BookData, b: BookData) => number;
// }
{
    const x = 'test'
    if (typeof x === 'number') { } // JS typeof => string ('number'|'string'|'boolean'|...)
    type typeofX = typeof x; // TS typeof X => Type of X
}

type BookSorter = typeof bookSorter;
type BookSorterComparator = ReturnType<typeof bookSorter['getComparator']>

const bookSorter = {
    dir: SortDir.DEFAULT,
    key: SortKey.DEFAULT,
    getComparator() {
        // return function (this: BookSorter, a: BookData, b: BookData): number {
        // return function (this: typeof bookSorter, a: BookData, b: BookData): number {
        return (a: BookData, b: BookData): number => { // === fn().bind(this)
            const strA = a.volumeInfo[this.key];
            const strB = b.volumeInfo[this.key];
            if (typeof strA === 'string') {
                return strA.localeCompare(String(strB), 'en', {}) * this.dir
            }
            if (typeof strA === 'number') {
                return String(strA).localeCompare(String(strB), 'en', { numeric: true }) * this.dir
            }
            if (Array.isArray(strA)) {
                // if (strA instanceof Array) {
                return String(strA[0]).localeCompare(String(strA[0]), 'en', { numeric: true }) * this.dir
            }
            const _never: never = strA
            throw new Error('Unsuported sort key type')
        }
        // }.bind(this)
    }
};

function formatYear(publishedDate: string): string {
    const date = new Date(publishedDate)
    if (date.toString() === 'Invalid Date') {
        return ' ? '
    }
    return date.getFullYear().toString()
}
