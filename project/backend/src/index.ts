import express, { Request, Response } from 'express';
import { router as booksRoutes } from './routes/books';


const app = express()
app.use(express.json())


app.get('/', (req, res) => {
    res.send('Hello')
})

app.use('/books', booksRoutes)


const server = app.listen(8080, 'localhost', () => {
    console.log('Server running on http://localhost:8080/ ')
})

