import { Router } from "express";
import { BookData, BookDataKind, CreateBookData, isBookData } from "../model/BookData";
import { createBook, findAllBooks, findBookById, queryBooks } from "../model/books";


export const router = Router()

router.get<{}, BookData[], null, { title?: string, author?: string, year?: string }>('/', (req, res) => {
    const { title } = req.query

    let result: BookData[]

    if (typeof title === 'string') {
        result = queryBooks({ type: 'title', title: title })
    } else {
        result = queryBooks({ 'type': 'all' })
    }

    res.send(result)
})

interface ErrorResponse {
    message: string;
}

router.get<{ bookId: string }, BookData | ErrorResponse, null, null>('/:bookId', (req, res) => {
    const id = req.params.bookId

    const result = findBookById(id)

    if (!result) {
        return res.status(404).send({ message: 'Not Found' })
    }

    res.send(result)
})

router.post<{}, BookData | ErrorResponse, BookData | {}, {}>('/', (req, res) => {
    const payload = req.body

    // if ('kind' in payload && payload.kind === BookDataKind) {
    //     createBook(payload.volumeInfo)
    // }

    if (!isBookData(payload)) {
        return res.status(401).send({ message: 'Bad request data' })
    } else {
        const result = createBook(payload.volumeInfo)
        res.send(result)
    }

})

